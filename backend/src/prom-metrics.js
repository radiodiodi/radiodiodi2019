const client = require('prom-client');

module.exports = {
  emailSentCounter: new client.Counter({
    name: 'diodi_email_sent_total',
    help: 'Number of emails sent since restart. Increases on errors too',
  }),
  emailErrorCounter: new client.Counter({
    name: 'diodi_email_errors_total',
    help: 'Number of email send failures since restart',
  }),
  messagesReceivedCounter: new client.Counter({
    name: 'diodi_ws_messages_received_total',
    help: 'Number of chat messages received since restart',
  }),
  messagesRelayedHistogram: new client.Histogram({
    name: 'diodi_ws_message_processing_time_seconds',
    help: 'Histogram of processing times of successfully processed messages',
  }),
  messagesDroppedCounter: new client.Counter({
    name: 'diodi_ws_messages_dropped',
    help: 'Number of chat messages dropped for reason since restart',
    labelNames: ['reason'],
  }),
  wsThrotleIncidents: new client.Counter({
    name: 'diodi_ws_throtle_incidents_total',
    help: 'Number of times client has been told to cool down',
  }),
  wsCurrentClients: new client.Gauge({
    name: 'diodi_ws_clients',
    help: 'Number of ws clients. Updated every 10 seconds',
  }),
  wsErrorsCounter: new client.Counter({
    name: 'diodi_ws_errors_total',
    help: 'Number of ws errors since restart',
  }),
  logLinesCounter: new client.Counter({
    name: 'diodi_log_lines_total',
    help: 'Number of lines logged. Labeled by severity',
    labelNames: ['severity'],
  }),
};
