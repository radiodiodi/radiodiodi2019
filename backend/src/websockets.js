const WebSocket = require('ws');
const models = require('./models');
const utils = require('./utils');
const rateLimiter = require('ws-rate-limit');
const {
  messagesReceivedCounter,
  messagesDroppedCounter,
  messagesRelayedHistogram,
  wsCurrentClients,
  wsErrorsCounter,
} = require('./prom-metrics');

const filterData = require('./filters');

const filters = {};
filterData.forEach(f => {
  filters[f.slug] = f;
});

const initializeFilters = async () => {
  const docs = await models.filters.find({}, 'slug');
  const slugsInDB = docs.map(d => d.slug);
  const slugsInCode = filterData.map(f => f.slug);

  // remove dangling slugs from database
  const danglingSlugs = slugsInDB.filter(slug => !slugsInCode.find(s => s === slug));
  danglingSlugs.forEach(slug => {
    models.filters
      .remove({ slug })
      .then(() => utils.info(`Removed dangling filter with slug ${slug}`))
      .catch(err => utils.error(err));
  });

  // insert new filters to database
  const slugsToInsert = slugsInCode.filter(slug => !slugsInDB.find(s => s === slug));
  slugsToInsert.forEach(slug => {
    const { order, description, defaultState } = filterData.find(f => f.slug === slug);
    models.filters
      .insert({
        slug,
        order,
        description,
        state: defaultState,
      })
      .then(() => utils.info(`Inserted filter to database with slug ${slug}`))
      .catch(err => utils.error(err));
  });
};

let wss;

const start = () => {
  utils.info(`Listening for Websockets on ${process.env.HOST} on port ${process.env.WS_PORT}.`);
  initializeFilters();

  wss = new WebSocket.Server({
    host: process.env.HOST,
    port: process.env.WS_PORT,
  });
  const rateLimit = rateLimiter('60s', 3);
  wss.on('connection', async (ws, req) => {
    const forwardedIP = req.headers['x-forwarded-for'];
    const ip = forwardedIP && forwardedIP !== '127.0.0.1' ? forwardedIP : req.connection.remoteAddress;
    const reserved = (await models.reserved.count({ ip })) !== 0;
    if (!reserved) rateLimit(ws);

    ws.on('message', async data => {
      messagesReceivedCounter.inc();
      const timerStop = messagesRelayedHistogram.startTimer();
      const { name, text } = JSON.parse(data);
      // utils.info(`Websocket: Received: "${text}" from "${name}"`);

      let message = {
        timestamp: new Date(Date.now()),
        name,
        text,
        ip,
        reserved,
      };

      const errorSender = (errorMessage) => {
        ws.send(JSON.stringify({
          message: {
            name: 'SERVER',
            error: true,
            timestamp: new Date(Date.now()),
            text: errorMessage,
          },
        }));
      };

      try {
        const activeFilters = await models.filters.find(
          { state: true },
          { fields: { slug: 1 }, sort: { order: 1 } },
        );
        const slugs = activeFilters.map(f => f.slug);
        for (const slug of slugs) {
          const filter = filters[slug].code;
          message = await filter(message, errorSender);
          if (!message) return;
        }
      } catch (err) {
        utils.error(`Error in message filtering: ${err}`);
        if (!message) return;
      }

      // perform final sanity check
      if (typeof (message.timestamp) !== 'object'
          || typeof (message.text) !== 'string'
          || typeof (message.name) !== 'string'
          || typeof (message.ip) !== 'string'
      ) {
        console.error(`Dropped invalid message object: ${message}`);
        return;
      }

      const dbMessage = await models.messages.insert(message);

      wss.clients.forEach((client) => {
        if (client.readyState === WebSocket.OPEN) {
          client.send(JSON.stringify({
            message: {
              name: message.name,
              text: message.text,
              timestamp: message.timestamp,
              _id: dbMessage._id,
              reserved: message.reserved,
            },
          }));
        }
      });
      timerStop();
    });

    const initial = (await models.messages.find({}, {
      sort: { timestamp: -1 }, limit: 100,
    })).reverse();

    ws.send(JSON.stringify({
      initial: initial.map(message => ({
        name: message.name,
        text: message.text,
        timestamp: message.timestamp,
        _id: message._id,
        reserved: message.reserved,
      })),
    }));

    ws.on('error', error => {
      wsErrorsCounter.inc();
      utils.error(`Websocket error. ${error}`);
    });

    ws.on('limited', () => {
      messagesReceivedCounter.inc();
      messagesDroppedCounter.labels('limited').inc();
      utils.warning(`User from ip "${req.connection.remoteAddress}" has been throttled.`);
      ws.send(JSON.stringify({
        message: {
          name: 'SERVER',
          text: 'Calm down, you are sending too many messages.',
          timestamp: new Date(Date.now()),
          error: true,
        },
      }));
    });
  });

  // update client count every 10 seconds
  setInterval(() => wsCurrentClients.set(Number(wss.clients.size)), 10000);
};

const eraseMessage = id => {
  wss.clients.forEach((client) => {
    if (client.readyState === WebSocket.OPEN) {
      client.send(JSON.stringify({
        erase: id,
      }));
    }
  });
};

module.exports = {
  start,
  eraseMessage,
};
