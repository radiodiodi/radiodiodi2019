const notEmptyField = (name, val) => {
  if (!val) {
    throw new Error(`${name} cannot be null.`);
  }
};

const validateRegistrationOrThrow = (data) => {
  notEmptyField('Name', data.name);
  notEmptyField('Email', data.email);
  notEmptyField('Description', data.description);
  notEmptyField('Genre', data.genre);
  notEmptyField('Duration', data.duration);
  notEmptyField('Participants', data.participants);
  notEmptyField('Producer', data.producer);
  notEmptyField('Photoshoot', data.photoshoot);
  notEmptyField('Responsible', data.responsible);
  notEmptyField('Team', data.team);
  notEmptyField('Time propositions', data.propositions);
  if (!data.propositions.length || data.propositions.length === 0) {
    throw new Error('Time propositions are empty.');
  }
  return true;
};

module.exports = { validateRegistrationOrThrow };
