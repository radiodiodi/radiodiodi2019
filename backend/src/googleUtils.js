const fs = require('fs');
const path = require('path');
const readline = require('readline');
const ejs = require('ejs');

const utils = require('./utils');

const { google } = require('googleapis');

const { emailSentCounter, emailErrorCounter } = require('./prom-metrics');

const SCOPES = [
  'https://www.googleapis.com/auth/calendar.readonly',
  'https://www.googleapis.com/auth/gmail.send',
];
const TOKEN_DIR = path.join(__dirname, '..', '.credentials');
const TOKEN_PATH = path.join(TOKEN_DIR, 'radiodiodi-nodejs-credentials.json');
const { CALENDAR_ID } = process.env;
const START_DATE = new Date(Date.parse('2019-04-12T00:00:00.000+03:00'));
const END_DATE = new Date(Date.parse('2019-05-01T00:00:00.000+03:00'));
const CALENDAR_INTERVAL = 1000 * 60 * 15; // 15 minutes

let calendarData = [];
let googleCredentials = null;

/* Parse event data from Google Calendar's event description.
   The editor must use this exact syntax:

   name
   ---
   description
   ---
   requires own producer? (needed only for editor's own notekeeping)
   ---
   genre/type of show
   ---
   full image link (https://.../image.png)
*/
function parseEventDescription(event) {
  const parts = event.description.split('---').map(p => p.replace(/\r?\n?/g, ''));
  return {
    ...event,
    team: parts[0],
    description: parts[1],
    genre: parts[3],
    image: parts[4],
  };
}

/**
 * Store token to disk be used in later program executions.
 *
 * @param {Object} token The token to store to disk.
 */
const storeToken = (token) => {
  try {
    fs.mkdirSync(TOKEN_DIR);
  } catch (err) {
    if (err.code !== 'EEXIST') {
      throw err;
    }
  }
  try {
    fs.writeFileSync(TOKEN_PATH, JSON.stringify(token));
    console.log(`Token stored to ${TOKEN_PATH}`);
  } catch (err) {
    console.error(err);
  }
};

const getNewToken = (oauth2Client) => new Promise((resolve, reject) => {
  const authUrl = oauth2Client.generateAuthUrl({
    access_type: 'offline',
    prompt: 'consent',
    scope: SCOPES,
  });
  console.log('\nAuthorize this app by visiting this url: ', authUrl);
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  });
  rl.question('\nEnter the code from that page here: ', (code) => {
    rl.close();
    oauth2Client.getToken(code, (err, token) => {
      if (err) {
        console.error('Error while trying to retrieve access token', err);
        reject(err);
      }
      oauth2Client.credentials = token; // eslint-disable-line no-param-reassign
      storeToken(token);
      resolve(oauth2Client);
    });
  });
});

const authorize = (credentials) => new Promise(async (resolve, reject) => {
  const clientSecret = credentials.web.client_secret;
  const clientId = credentials.web.client_id;
  const redirectUrl = credentials.web.redirect_uris[0];

  let oauth2Client;
  try {
    oauth2Client = new google.auth.OAuth2(clientId, clientSecret, redirectUrl);
  } catch (err) {
    console.error(err);
    reject(err);
  }

  // Check if we have previously stored a token.
  try {
    const token = fs.readFileSync(TOKEN_PATH);
    oauth2Client.credentials = JSON.parse(token);
    resolve(oauth2Client);
  } catch (err) {
    console.error(err);
    resolve(getNewToken(oauth2Client));
  }
});

const fetchCalendarEvents = (payload, auth) => new Promise((resolve, reject) => {
  const calendar = google.calendar({ version: 'v3', auth });
  calendar.events.list(payload, (err, res) => {
    if (err) reject(err);
    else resolve(res);
  });
});

/**
 * Lists the next 10 events on the user's primary calendar.
 *
 * @param {google.auth.OAuth2} auth An authorized OAuth2 client.
 */
const listEvents = async (auth) => {
  const payload = {
    calendarId: CALENDAR_ID,
    timeMin: START_DATE.toISOString(),
    timeMax: END_DATE.toISOString(),
    singleEvents: true,
    orderBy: 'startTime',
  };

  let response;
  try {
    response = await fetchCalendarEvents(payload, auth);
  } catch (err) {
    console.error(err);
    return [];
  }

  const events = response.data.items || [];

  const newCalendar = events.map(event => {
    const title = event.summary;
    const description = event.description || '';
    const start = event.start.dateTime || event.start.date;
    const end = event.end.dateTime || event.end.date;
    const id = event.id || null;
    const result = parseEventDescription({
      id,
      title,
      description,
      start,
      end,
    });
    return result;
  });

  return newCalendar;
};

const initCalendarReader = () => {
  const readCalendar = async () => {
    try {
      const client = await authorize(googleCredentials);
      const results = await listEvents(client);

      calendarData = results;
      utils.info(`Updated calendar. Fetched ${results.length} results.`);
    } catch (err) {
      console.error(err);
      utils.error('Error while authorizing calendar.');
    }
  };

  readCalendar();

  setInterval(readCalendar, CALENDAR_INTERVAL);
};

const makeBody = ({ to, cc, bcc, from, subject, message }) => {
  const str = ['Content-Type: text/html; charset="UTF-8"\n',
    'MIME-Version: 1.0\n',
    'Content-Transfer-Encoding: 7bit\n',
    'To: ', to, '\n',
    'Cc: ', cc, '\n',
    'Bcc: ', bcc, '\n',
    'From: ', from, '\n',
    'Subject: ', subject, '\n\n',
    message,
  ].join('');

  const encodedMail = Buffer.from(str).toString('base64').replace(/\+/g, '-').replace(/\//g, '_');
  return encodedMail;
};

const htmlTemplate = fs.readFileSync('./resource/email.html', 'utf8');
const sendGmailConfirmation = async (formParams) => {
  const html = ejs.render(htmlTemplate, { data: formParams });
  const data = {
    to: formParams.email,
    bcc: 'ohjelmantekijailmot@radiodiodi.fi',
    from: 'radiodiodi@radiodiodi',
    subject: 'Radiodiodi - Kiitos ilmoittautumisesta!',
    message: html,
  };

  const raw = makeBody(data);
  const auth = await authorize(googleCredentials);
  const gmail = google.gmail({ version: 'v1', auth });

  const payload = {
    auth,
    userId: 'me',
    resource: {
      raw,
    },
  };

  gmail.users.messages.send(payload, (err) => {
    if (err) {
      emailErrorCounter.inc();
      console.log(`The Gmail API returned an error: ${err}`);
    } else {
      emailSentCounter.inc();
      console.log(`Successfully sent email through Gmail API to ${data.to}`);
    }
  });
};

googleCredentials = JSON.parse(fs.readFileSync('client_secret.json'));
initCalendarReader();

const getCalendarData = () => calendarData;

module.exports = {
  getCalendarData,
  sendGmailConfirmation,
};
