const utils = require('./utils');
const models = require('./models');
const metrics = require('./prom-metrics');
const geoip = require('geoip-lite');

const MAX_MESSAGE_LENGTH = 500;
const MAX_USERNAME_LENGTH = 16;

/*
 * A good filter:
 *  - Returns message object if message is valid
 *  - Returns null if not
 *  - Lets messages pass by default, if there is problem in filer execution
 */

const disableChatFilter = async (message, errorSender) => {
  errorSender('Chat is not available at the moment');
  metrics.messagesDroppedCounter.labels('chat_disabled').inc();
  return null;
};

const reservedFilter = async (message, errorSender) => { // eslint-disable-line
  let reserved = false;
  try {
    reserved = (await models.reserved.count({ ip: message.ip })) !== 0;
  } catch (error) {
    utils.warning(`Error when querying reserved ips: ${error}`);
  }
  return { ...message, reserved };
};

const messageLengthFilter = async (message, errorSender) => {
  if (message.text.length > MAX_MESSAGE_LENGTH) {
    errorSender(`Message too long. Max length is ${MAX_MESSAGE_LENGTH} characters.`);
    metrics.messagesDroppedCounter.labels('message_too_long').inc();
    return null;
  }
  return message;
};

const usernameLengthFilter = async (message, errorSender) => {
  if (message.name.length > MAX_USERNAME_LENGTH) {
    errorSender(`Username too long. Max length is ${MAX_USERNAME_LENGTH} characters.`);
    metrics.messagesDroppedCounter.labels('username_too_long').inc();
    return null;
  }
  return message;
};

const bannedIpFilter = async (message, errorSender) => {
  try {
    const banned = await models.bans.findOne({ ip: message.ip });
    if (banned) {
      errorSender('You are banned.');
      metrics.messagesDroppedCounter.labels('user_banned').inc();
      return null;
    }
  } catch (error) {
    utils.warning(`Error when querying banned ips: ${error}`);
  }
  return message;
};

const bannedWordFilter = async (message, errorSender) => {
  try {
    const bannedWords = await models.bannedWords.find({});
    for (const entry of bannedWords) {
      if (message.text.includes(entry.word)) {
        errorSender('You said something naughty.');
        metrics.messagesDroppedCounter.labels('bad_word');
        return null;
      }
    }
  } catch (error) {
    utils.warning(`Error when querying banned words: ${error}`);
  }
  return message;
};

const geoblockFilter = async (message, errorSender) => {
  if (message.reserved) return message;
  const lookupResult = geoip.lookup(message.ip);
  const inFinland = lookupResult && lookupResult.country === 'FI';
  if (!inFinland && message.ip !== '127.0.0.1') {
    utils.info(`Dropping message from IP ${message.ip} based on geoip lookup`);
    errorSender('Messages from outside of Finland not allowed to deter spamming.');
    metrics.messagesDroppedCounter.labels('geoblock').inc();
    return null;
  }
  return message;
};

const filters = [
  {
    slug: 'chat-disabled',
    description: 'Disable chat completely',
    order: 1,
    defaultState: true,
    code: disableChatFilter,
  }, {
    slug: 'reserved-filter',
    description: 'Verify reserved users.',
    order: 3,
    defaultState: true,
    code: reservedFilter,
  }, {
    slug: 'geoblock',
    description: 'Drop messages originating from abroad',
    order: 5,
    defaultState: false,
    code: geoblockFilter,
  }, {
    slug: 'banned-ip',
    description: 'Check user ip against banned ips',
    order: 10,
    defaultState: true,
    code: bannedIpFilter,
  }, {
    slug: 'message-length',
    description: 'Limit message length',
    order: 20,
    defaultState: true,
    code: messageLengthFilter,
  }, {
    slug: 'username-length',
    description: 'Limit username length',
    order: 30,
    defaultState: true,
    code: usernameLengthFilter,
  }, {
    slug: 'banned-word',
    description: 'Drop messages containing banned words',
    order: 50,
    defaultState: true,
    code: bannedWordFilter,
  },
];

module.exports = filters;
