module.exports = {
    "extends": "airbnb-base",
    "rules": {
      "arrow-parens": "off",
      "one-var": "off",
      "one-var-declaration-per-line": "off",
      "no-console": "off",
      "import/no-unresolved": "off",
      "no-plusplus": "off",
      "no-underscore-dangle": "off",
      "object-curly-newline": "off",
      "no-restricted-syntax": "off",
      "no-await-in-loop": "off",
    }
};