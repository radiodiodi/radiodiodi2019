import React from 'react';
import styled from 'styled-components';
import { shortenText } from '../../utils'
// import placeholderImg from '../../images/placeholder_dark.svg'
import dateFormat from 'dateformat';

const ProgramBlock = styled.div`
  padding: 1rem;
  min-height: ${p => p.maintainance ? 'none' : '150px'};

  &::after {
    content: "";
    clear: both;
    display: table;
  }

  background-color: ${p => p.theme.color.white10};
  margin-bottom: 0.5rem;
  display: flex;
  flex-direction: row;

  @media screen and (max-width: 500px) {
    flex-direction: column;
  }

  justify-content: space-between;

  @media screen and (max-width: 700px) {
    display: ${p => p.oneDayPreview ? 'none' : 'flex'};
  }
`;

const Column = styled.div`
  flex: ${p => p.flex};
  display: flex;
  flex-direction: column;
`;

const Image = styled.img`
  height: 150px;
  width: 150px;
  background-size: cover;
  margin-right: 2rem;

  @media screen and (max-width: 500px) {
    margin: 0 2rem 2rem 0;
    align-self: flex-start;
  }
`;

const Title = styled.h4`
  margin: 0.5rem 0;
  font-size: 18px;
  border-bottom: 1px solid ${p => p.theme.color.pink};
  word-wrap: break-word;

  @media (max-width: 600px) {
    margin-right: 0;
  }
`;

const Genre = styled.small`
  margin-top: -4px;
`;

const Author = styled.p`
  margin: 0.5rem 0;
`;

const Paragraph = styled.p`
  font-size: 12px;

  @media screen and (max-width: 800px - 1px) {
    font-size: 16px !important;
  }
`;

const ShowMore = styled.span`
  cursor: pointer;
  font-size: 12px;
  padding: 0;

  @media screen and (max-width: 800px - 1px) {
    font-size: 16px;
  }
`;

const PlaceholderImage = ({ alt }) => (
  <Image
    src={`${process.env.REACT_APP_STATIC_URL}/img/2019/toimitus/thumbnail/kiwi.jpg`}
    alt={alt}
  />
);

const ProgramImage = ({ src, alt }) => {
  const originalHref = src.replace('/ohjelmat', '/ohjelmat/original');

  return (
    <a href={originalHref}>
      <Image
        src={src}
        alt={alt}
      />
    </a>
  );
}

const BlueSpan = styled.span`
  color: ${p => p.theme.color.blue100};
`;

const DESCRIPTION_MAX_LENGTH = 200;
const ExpandTextLink = ({ expandedText, toggleExpand }) => (
  <ShowMore onClick={toggleExpand}>
    {expandedText}
  </ShowMore>
);

class Program extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      expanded: false
    }
    this.toggleExpand = this.toggleExpand.bind(this);
  }

  toggleExpand() {
    this.setState({ expanded: !this.state.expanded });
  }

  renderDescription = () => {
    const { p } = this.props;
    const { expanded } = this.state;

    const expandedText = (
      <BlueSpan>
        {expanded ? ' Vähemmän' : ' Lisää'}
      </BlueSpan>
    );

    const expandLink = p.description.length > DESCRIPTION_MAX_LENGTH
      ? <ExpandTextLink expandedText={expandedText} toggleExpand={this.toggleExpand} />
      : null;

    return (
      <Paragraph>
        {this.state.expanded ? p.description : shortenText(p.description, DESCRIPTION_MAX_LENGTH)}
        {expandLink}
      </Paragraph>
    );
  }

  render() {
    const { p, oneDayPreview } = this.props;
    const maintainance = p.title === 'HUOLTOTAUKO';
    const img = p.image && p.image.trim()
      ? <ProgramImage src={p.image} alt={p.title} />
      : <PlaceholderImage alt={p.title} />;

    const startDate = dateFormat(p.start, 'HH:MM');
    const endDate = dateFormat(p.end, 'HH:MM');

    const description = p.description
      ? this.renderDescription()
      : null;

    return (
      <ProgramBlock oneDayPreview={oneDayPreview} maintainance={maintainance}>
        <Column>
          {img}
        </Column>
        <Column flex={1}>
          <Genre>{p.genre}</Genre>
          <small>{`${startDate} - ${endDate}`}</small>
          <Title>{p.title}</Title>
          <Author>{p.team}</Author>
          {description}
        </Column>
      </ProgramBlock>
    )
  }
}

export default Program
