import React from 'react';
import styled from 'styled-components'
import Program from './Program'

import dotenv from 'dotenv';
dotenv.config();

const { REACT_APP_CALENDAR_ID } = process.env;

const Button = styled.button`
  background-color: ${p => p.theme.color.pink75};
  color: ${p => p.theme.color.white100};
  padding: 0.5rem 0;
  font-size: 14px;
  border: none;
  min-width: 100px;
  cursor: pointer;

  @media screen and (max-width: 450px) {
    margin: 0.5rem 0;
  }
`;

const Controls = styled.div`
  margin: 1rem 0;
  padding: 0.5rem 0;
  text-align: center;
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
  flex-direction: row;

  @media screen and (max-width: 450px) {
    flex-direction: column;
  }
`;

const CalendarLink = styled.div`
  text-align: center;
  margin-bottom: 2rem;
`;

const groupBy = (xs, key) => xs
  .reduce((rv, x) => {
    var v = key instanceof Function ? key(x) : x[key];
    (rv[v] = rv[v] || []).push(x);
    return rv;
  }, {});

const WEEKDAYS = [
  'Maanantai', 'Tiistai', 'Keskiviikko', 'Torstai', 'Perjantai', 'Lauantai', 'Sunnuntai',
];

class Calendar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ready: false,
    }

    this.fetchProgrammes();
  }

  incrementDay = () => {
    this.setState(({ today }) => ({ today: Math.min(30, today + 1) }))
  }

  decrementDay = () => {
    this.setState(({ today }) => ({ today: Math.max(15, today - 1) }))
  }

  fetchProgrammes = async () => {
    try {
      const resp = await fetch(`${process.env.REACT_APP_BACKEND_HTTP_URL}/programmes`)
      const data = await resp.json();
      if (!data || !Array.isArray(data)) {
        console.log('Programme data null. Data:');
        console.log(data);
        return;
      }

      const images = data.map(p => p.image);
      this.preloadImages(images);

      const r = data.sort((x, y) => + Date.parse(x.start) - Date.parse(y.start));
      const grouped = groupBy(r, (x) => {
        return x.start.substr(8, 2);
      });
      this.setState({
        today: Math.max((new Date()).getDate(), 15),
        all: grouped,
        ready: true,
      });
    } catch (error) {
      console.log(error);
    }
  }

  preloadImages = async (images) => {
    images.forEach((src) => {
      const img = document.createElement('img');
      img.src = src; // Assigning the img src immediately requests the image
    });
  }

  render() {
    const { oneDayPreview } = this.props;
    const { ready, all, today} = this.state;

    const dayOfTheWeek = WEEKDAYS[(today - 1) % 7];
    const calendarControls = <Controls>
      <Button onClick={this.decrementDay}>Edellinen</Button>
      <span>{dayOfTheWeek} {today}.4.2019</span>
      <Button onClick={this.incrementDay}>Seuraava</Button>
    </Controls>

    if (!ready) return null;

    if (oneDayPreview) {
      if (all[today]) {
        const currentProgram = all[today].find(program => {
          const now = new Date();
          const start = new Date(Date.parse(program.start));
          const end = new Date(Date.parse(program.end));
          const hasNotEnded = end > now;
          const hasStarted = start < now;
          return hasStarted && hasNotEnded;
        });

        if (!currentProgram) {
          return null;
        }
        return <Program oneDayPreview p={currentProgram} />;
      } else {
        return null;
      }
    }

    const content = all[today]
      ? all[today].map(p => <Program p={p} key={p.id || p.start} />)
      : [];

    return (
      <div>
        <h2 id="calendar">Ohjelmakalenteri</h2>
        {calendarControls}
        {content}
        {calendarControls}
        <CalendarLink>
          Kalenteri on luettavissa kokonaisuudessaan täällä:&nbsp;
          <a href={`https://calendar.google.com/calendar/b/4/r?cid=${REACT_APP_CALENDAR_ID}`}>
            Google Calendar
          </a>
        </CalendarLink>
      </div>
    )
  }
}

export default Calendar
