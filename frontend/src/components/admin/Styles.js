import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  flex-wrap: nowrap;
`;

export const Column = styled.div`
  ${ p => p.hidden && 'display: none'};
  margin: 0.5rem 0;
  flex: 1 0 auto;

  @media screen and (min-width: 500px) {
    min-width: 400px;
  }

  max-width: 100%;
`;

export const Log = styled.div`
  max-height: 20rem;
  overflow-y: auto;
  overflow-x: hidden;
  margin-bottom: 2rem;
`;

export const Count = styled.div`
  margin: 0.5rem 0;
`;

export const LogRow = styled.div`
  margin: 0.5rem 0;
  cursor: pointer;

  &:hover {
    padding-left: 8px;
    border-left: 4px solid ${p => p.theme.color.blue100};
  }

  transition: 0.5s padding-left;
  transition: 0.1s border-left;
  min-width: 0;
`;

export const LogRowContrast = styled.span`
  color: ${p => p.theme.color.blue100};
`;
