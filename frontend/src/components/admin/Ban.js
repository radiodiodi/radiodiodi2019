import React, { Component } from 'react';
import { LogRow } from './Styles';

class Ban extends Component {
  render() {
    const { data, onSelect } = this.props;
    return (
      <LogRow onClick={() => onSelect(data, 'ban')}>
        { data.name }, IP: { data.ip }
      </LogRow>
    );
  }
}

export default Ban;