import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router';
import LogoutButton from './LogoutButton';

const Row = styled.div`
  display: flex;
  flex-flow: row nowrap;
  align-items: center;

  a {
    margin-right: 0.5rem;
  }

  @media screen and (max-width: 600px) {
    flex-flow: column nowrap;
    align-items: flex-end;

    a,
    button {
      margin-bottom: 0.5rem;
    }
  }
`;

const AdminNavbar = () => {
  return (
    <Row>
      <Link to="/admin/shoutbox">Shoutbox</Link>
      <Link to="/admin/bans">Bans</Link>
      <Link to="/admin/reserved_users">Reserved users</Link>
      <Link to="/admin/registrations">Registrations</Link>
      <LogoutButton />
    </Row>
  );
};

export default withRouter(AdminNavbar);
