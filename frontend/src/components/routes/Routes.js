import React, { Fragment, Component } from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import Header from '../common/Header';
import Footer from '../common/Footer';
import Background from '../common/Background';
import Frontpage from '../pages/frontpage/Frontpage';
import Sponsors from '../pages/sponsors/Sponsors';
// import Registration from '../pages/registration/Registration';
import Library from '../pages/library/Library';
import Login from '../pages/admin/Login';
import Guide from '../pages/guide/Guide';
import Separator from '../common/Separator';
import English from '../pages/english/English';
import CalendarCallback from '../pages/calendar-callback/CalendarCallback';
// import RegistrationSuccess from '../pages/registration/Success';
// import RegistrationError from '../pages/registration/Error';
import AdminRegistrationsPage from '../pages/admin/AdminRegistrationsPage';
import AdminShoutboxPage from '../pages/admin/AdminShoutboxPage';
import AdminBansPage from '../pages/admin/AdminBansPage';
import AdminReservedPage from '../pages/admin/AdminReservedPage';

class Routes extends Component {
  render() {
    return (
      <Router>
        <Fragment>
          <Background />
          <Header />
          <Separator />
          <Switch>
            <Route exact path="/" component={Frontpage} />
            <Route path="/sponsors" component={Sponsors} />
            {/* <Route path="/ilmo/success" component={RegistrationSuccess} /> */}
            {/* <Route path="/ilmo/error" component={RegistrationError} /> */}
            {/* <Route path="/ilmo" component={Registration} /> */}
            <Route path="/guide" component={Guide} />
            <Route path="/admin/bans" component={AdminBansPage} />
            <Route path="/admin/registrations" component={AdminRegistrationsPage} />
            <Route path="/admin/reserved_users" component={AdminReservedPage} />
            <Route path="/admin/shoutbox" component={AdminShoutboxPage} />
            <Redirect from="/admin" to="/admin/shoutbox" />
            <Route path="/login" component={Login} />
            <Route path="/library" component={Library} />
            <Route path="/english" component={English} />
            <Route path="/callback" component={CalendarCallback} />
            <Route component={() => <h1>404</h1>} />
          </Switch>
          <Footer />
        </Fragment>
      </Router>
    );
  }
}

export default Routes;
