import React, { Component } from 'react';
import styled from 'styled-components';
import { fetchSongsByField, fetchSongs } from '../../../utils';

const Container = styled.div`
  p {
    max-width: 700px;
  }
`;

const Results = styled.div`
`;

const Result = styled.div`
  color: ${p => p.header ? p.theme.color.blue100 : p.theme.color.white100};
  background-color: ${p => p.contrast ? p.theme.color.white10 : 'none'};
  padding: 0.5rem 0.3rem;
  margin: 0.2rem 0;
  display: flex;
  flex-direction: row;

  @media screen and (max-width: 700px) {
    flex-direction: column;
  }
`;

const Error = styled.h2`
  text-align: center;
  font-weight: lighter;
  color: ${p => p.theme.color.pink100};
`;

const Column = styled.div`
  flex: 1;
  margin-right: 0.2rem;
`;

const SearchBar = styled.input`
  padding: 0.5rem;
  flex: 1;
`;

const SearchContainer = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  margin-bottom: 2rem;
`;

const SearchButton = styled.a`
  padding: 0.5rem;
  min-width: 5rem;
  text-align: center;
  cursor: pointer;
`;

const TypePickerContainer = styled.div`
  display: flex;
  justify-content: flex-start;
  margin: 1rem 0;
`;

const TypeLabel = styled.label`
  display: flex;
  flex-direction: row;
  align-items: center;
  margin-right: 1rem;

  label {
    padding-right: 0.5rem;
  }
`;

const TypePicker = styled.input`
`;


const AnnouncementBox = styled.div`
  text-align: center;
  padding: 0 2rem 2rem;
  margin-bottom: 2rem;
  border-bottom: solid;
  border-width: 2px;

  @media screen and (max-width: 700px) {
    margin-top: 2rem;
  }
`;

class Library extends Component {
  state = {
    results: [],
    type: 'title',
    initialLoadDone: false,
  }

  fetchAll = async () => {
    try {
      const songs = await fetchSongs();
      return songs;
    } catch (err) {
      console.log(err);
      return [];
    }
  }

  loadInitial = async () => {
    const results = await this.fetchAll();
    this.setState({
      results,
      initialLoadDone: true,
    });
  }


  componentDidMount() {
    this.loadInitial();
  }

  fetchBy = async (type, value) => {
    try {
      if (value.length > 0) {
        return await fetchSongsByField(type, value);
      } else {
        return await this.fetchAll();
      }
    } catch (err) {
      console.log(err);
      return [];
    }
  }

  fetchByTitle = async title => {
    const results = await this.fetchBy('title', title);
    this.setState({
      results,
    });
  }

  fetchByArtist = async artist => {
    const results = await this.fetchBy('artist', artist);
    this.setState({
      results,
    });
  }

  fetchByAlbum = async album => {
    const results = await this.fetchBy('album', album);
    this.setState({
      results,
    });
  }

  renderResult = (result, index) => {
    if (!result || !result.title) return null;

    const artistData = Array.isArray(result.artist)
      ? result.artist.join(', ')
      : result.artist;

    const even = index % 2 === 0;

    return (
      <Result white key={index} contrast={even}>
        <Column>{ result.title }</Column>
        <Column>{ artistData }</Column>
        <Column>{ result.album }</Column>
      </Result>
    );
  }

  renderHeaders = () => {
    return (
      <Result header>
        <Column>Kappale</Column>
        <Column>Artisti</Column>
        <Column>Albumi</Column>
      </Result>
    );
  }

  renderNotFound = () => {
    return (
      <Error>
        Ei hakutuloksia :(
      </Error>
    )
  }

  renderResults = results => {
    const rows = results.map((r, i) => this.renderResult(r, i))
    return (
      <Results>
        { this.renderHeaders() }
        { rows }
      </Results>
    );
  }

  renderOutOfOrder = () => {
    return (
      <AnnouncementBox>
        <Error>Musiikkikirjastoa päivitetään parhaillaan, eikä haku ole tällä hetkellä käytössä. Pahoittelut häiriöstä.</Error>
      </AnnouncementBox>
    );
  }

  onInput = () => {
    const value = this.input.value;
    const { type } = this.state;

    if (type === 'title') {
      this.fetchByTitle(value);
    } else if (type === 'artist') {
      this.fetchByArtist(value);
    } else if (type === 'album') {
      this.fetchByAlbum(value);
    }
  }

  onPromptKeyPress = event => {
    const ENTER_KEY = 13;

    if ([event.keyCode, event.which].includes(ENTER_KEY)) {
      this.onInput();
    }
  }

  onChangeType = type => {
    this.setState({
      type,
    });
  }

  renderNotLoaded = () => {
    return (
      <Error>
        Musiikkia haetaan...
      </Error>
    )
  }

  renderContent = () => {
    const { results, initialLoadDone } = this.state;

    if (!initialLoadDone) {
      return this.renderNotLoaded();
    }

    if (results.length === 0) {
      return this.renderNotFound();
    }

    return this.renderResults(results);
  }

  render() {
    const { type } = this.state;
    const content = this.renderContent();

    return (
      <Container>
        <h1>Musiikkikirjasto</h1>
        <p>Radiodiodilla on käytössään musiikkikirjasto, jonka musiikkia voi käyttää lähetyksessä.
        Tarkista löytyykö lempikappaleesi kirjastosta! Sivulla näytetään max 50 hakutulosta.</p>
        <TypePickerContainer>
          <TypeLabel>
            <label>Kappale</label>
            <TypePicker type="radio" name="type" onChange={() => this.onChangeType('title')} checked={type === 'title'}/>
          </TypeLabel>
          <TypeLabel>
            <label>Artisti</label>
            <TypePicker type="radio" name="type" onChange={() => this.onChangeType('artist')} checked={type === 'artist'}/>
          </TypeLabel>
          <TypeLabel>
            <label>Albumi</label>
            <TypePicker type="radio" name="type" onChange={() => this.onChangeType('album')} checked={type === 'album'}/>
          </TypeLabel>
        </TypePickerContainer>
        <SearchContainer>
          <SearchBar onKeyPress={this.onPromptKeyPress} innerRef={i => { this.input = i }} type="text" />
          <SearchButton onClick={this.onInput}>Hae</SearchButton>
        </SearchContainer>
        { content }
      </Container>
    );
  }
}


export default Library;