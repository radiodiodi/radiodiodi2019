import React, { Component } from 'react';
import { withRouter } from "react-router";
import axios from 'axios';
import {
  Form as InformedForm,
  Text as InformedText,
  TextArea as InformedTextArea,
  RadioGroup as InformedRadioGroup,
  Radio } from 'informed';
import styled from 'styled-components';

import PropositionPicker from '../../common/PropositionPicker';

const Form = styled(InformedForm)`
  * {
    font-size: 16px;
  }
`;

const Link = styled.a`
`;

const StyledText = styled(InformedText)`
  padding: 0.5rem;
  max-width: 500px;
  width: 100%;
`;

const StyledTextArea = styled(InformedTextArea)`
  padding: 0.5rem;
  max-width: 500px;
  min-height: 100px;
  width: 100%;
`;

const Label = styled.label`
  display: block;
  padding: 0.5rem 0.5rem 0.5rem 0;

  input:not([type="checkbox"]):not([type="radio"]),
  textarea,
  button {
    display: block;
  }
`;

const Button = styled.button`
  padding: 0.7rem 1.2rem;
  margin: 0.5rem 0;
  background-color: ${p => p.theme.color.blue100};
  color: ${p => p.theme.color.white100};
  border: none;
  outline: none;
  cursor: pointer;
`;

const BACKEND_URL = process.env.REACT_APP_BACKEND_HTTP_URL;

const notEmptyString = value => !value ? 'Kenttää ei voi jättää tyhjäksi' : undefined;
const validatePropositions = propositions => {
  if (!propositions || propositions.length === 0) {
    return 'Ehdota vähintään yhtä aikaväliä';
  }

  for (const p of propositions)  {
    if (!p.date) {
      return 'Ainakin yhden aikaehdotuksen päivämäärä on asettamatta';
    }
    if (!p.startTime) {
      return 'Ainakin yhden aikaehdotuksen aloituskellonaika on asettamatta';
    }
    if (!p.endTime) {
      return 'Ainakin yhden aikaehdotuksen lopetuskellonaika on asettamatta';
    }
  };

  return undefined;
}

const Text = (props) => (
  <StyledText validate={notEmptyString} {...props} />
);

const TextArea = (props) => (
  <StyledTextArea validate={!props.optional && notEmptyString} {...props} />
);

const RadioGroup = (props) => (
  <InformedRadioGroup validate={notEmptyString} {...props} />
);

const Error = styled.div`
  color: ${p => p.theme.color.pink100};
`;

class RegistrationForm extends Component {
  setFormApi = (formApi) => {
    this.formApi = formApi;
  }

  handleSubmit = async () => {
    const { history } = this.props;
    const state = this.formApi.getState();
    console.log(state);

    try {
      // remove null propositions
      state.values.propositions = state.values.propositions.filter(p => p.date);
      const resp = await axios.post(`${BACKEND_URL}/api/register`, state.values);
      console.log(resp);
      const email = state.values.email;
      history.push(`/ilmo/success?email=${email}`);
    } catch (err) {
      console.error(err);
      history.push('/ilmo/error');
    }
  }

  getErrors = () => this.formApi
    ? this.formApi.getState().errors || {}
    : {};

  getValues = () => this.formApi
    ? this.formApi.getState().values || {}
    : {};

  renderErrors = () => {
    const errors = this.getErrors();
    const hasErrors = !!Object.keys(errors).length;
    if (!hasErrors) return null;

    return (
      <Error>
        Lomakkeessa on virheitä. Tarkista vielä kenttien tiedot!
      </Error>
    )
  }

  rerender = () => {
    // force rerender
    this.setState({});
  }

  handleSubmitFailure = (data) => {
    this.rerender();
    return true;
  }

  setPropositionValues = (value) => {
    const formApi = this.formApi;
    if (formApi) formApi.setValue('propositions', value);
  }

  getPropositionValues = () => {
    const formApi = this.formApi;
    const val = formApi ? formApi.getValue('propositions') : [];
    return val || [];
  }

  render() {
    const errors = this.getErrors();
    const values = this.getValues();

    return (
      <Form getApi={this.setFormApi}
            onSubmit={this.handleSubmit}
            onSubmitFailure={this.handleSubmitFailure}
            initialValues={{
              propositions: [],
            }}>
        <Label>
          Ohjelman nimi
          {errors.name && <Error>{errors.name}</Error>}
          <Text
            field="name"
            placeholder="Kevätkarnevaaliradio" />
        </Label>
        <Label>
          Ohjelman kuvaus
          {errors.description && <Error>{errors.description}</Error>}
          <TextArea
            field="description"
            placeholder="Miten markkinoisit ohjelmaasi radion kuuntelijoille? Tämä tulee näkyviin ohjelmakalenteriin sen julkaisun jälkeen. Maksimissaan 400 merkkiä."
          />
        </Label>
        <Label>
          Ohjelmantekijän tai tiimin nimi
          {errors.team && <Error>{errors.team}</Error>}
          <Text
            field="team"
            placeholder="Ossi Ohjelmantekijä ja negatiiviset nopat" />
        </Label>
        <Label>
          Vastuuhenkilö
          {errors.responsible && <Error>{errors.responsible}</Error>}
          <Text
            field="responsible"
            placeholder="Oma nimi"
          />
        </Label>
        <Label>
          Vastuuhenkilön sähköpostiosoite
          {errors.email && <Error>{errors.email}</Error>}
          <Text
            field="email"
            type="email"
            placeholder="email@example.com"
          />
        </Label>
        <Label>
          Ohjelman tyyppi
          {errors.genre && <Error>{errors.genre}</Error>}
          <Text
            field="genre"
            placeholder="Huumori ja keskustelu" />
        </Label>
        <Label>
          Osallistujien (arvioitu) määrä
          {errors.participants && <Error>{errors.participants}</Error>}
          <Text
            field="participants"
            type="number"
            min="1"
            placeholder="Studioon mahtuu luontevasti noin 6 + tuottaja" />
          <p>Jos ohjelmaasi on aikeissa tulla suuri joukko ihmisiä, keskustele ensin Radiodiodin toimituksen kanssa.</p>
        </Label>
        <Label>
          Ohjelman kesto (tunneissa)
          {errors.duration && <Error>{errors.duration}</Error>}
          <Text field="duration" type="number" min="1" max="24" placeholder="2" />
          <p>Tarkista ohjelmasi keston järkevyys <a target="_blank" href="/guide/programme">ohjelmalaskurilla</a>!</p>
        </Label>
        <Label>
          Lisätietoja
          {errors.info && <Error>{errors.info}</Error>}
          <TextArea
            field="info"
            placeholder="Kysymyksiä toimitukselle? Erityisiä toiveita lähetykseen liittyen? Muita toiveita? Tänne vaan."
            optional
          />
        </Label>
        <Label>
          Tuottaja
          {errors.producer && <Error>{errors.producer}</Error>}
          <RadioGroup
            field="producer"
            onValueChange={this.rerender}>
            <Label>
              <Radio value="oma tuottaja" />
              Minulla on tuottaja, joka aikoo käydä tuottajakoulutuksessa
            </Label>
            { values.producer === 'oma tuottaja' && (
              <p>Tuottajien tulee käydä Radiodiodin studiotiimin järjestämässä tuottajakoulutuksessa.
                 Laita viestiä osoitteeseen <Link href="mailto:studio@radiodiodi.fi">studio@radiodiodi.fi</Link> ja liity
                 virallisille Telegram-kanaville, jottet missaa tärkeää tiedotusta!</p>
            )}
            <Label>
              <Radio value="ei omaa tuottajaa" />
              Tarvitsen tuottajan
            </Label>
          </RadioGroup>
        </Label>
        <p>Valitse alta kaikki sopivat aikavälit ohjelmallesi. Aikavälit eivät merkkaa ohjelmasi kestoa, vaan sitä, milloin voit pitää ohjelmasi.</p>
        {errors.propositions && <Error>{errors.propositions}</Error>}
        <PropositionPicker
          field="propositions"
          validate={validatePropositions}
          getValues={this.getPropositionValues}
          setValues={this.setPropositionValues}
        />
        <p>Pyrimme kuvaamaan kaikki ohjelmantekijät ohjelmakalenteria varten.
        Valokuvaustilaisuus järjestetään 3.4. ja 4.4. myöhemmin ilmoitettavassa paikassa.</p>
        <Label>
          Valokuvausaika
          {errors.photoshoot && <Error>{errors.photoshoot}</Error>}
          <RadioGroup
            field="photoshoot"
            onValueChange={this.rerender}>
            <Label>
              <Radio value="3.4." />
              3.4. klo 18
            </Label>
            <Label>
              <Radio value="4.4." />
              4.4. klo 18
            </Label>
            <Label>
              <Radio value="oma kuva" />
              Kuvaan julkaisukelpoisen kuvan itse
            </Label>
            { values.photoshoot === 'oma kuva' && (
              <p>Jos haluat kuvata oman kuvasi, varmista, että se toimii myös 1:1-kuvasuhteessa (neliö) ja että se on korkealaatuinen valotuksen sekä kuvanlaadun osalta.</p>
            )}
            <Label>
              <Radio value="mikään ei sovi" />
              Valitettavasti mikään vaihtoehto ei sovi
            </Label>
            {values.photoshoot === 'mikään ei sovi' && (
              <p>Harmin paikka! Käytämme ohjelmasi kuvana oletusgrafiikkaa.</p>
            )}
          </RadioGroup>
        </Label>
        <p>Jos jäi kysymyksiä, vastauksia löytyy postilokerosta <Link href="mailto:toimitus@radiodiodi.fi">toimitus@radiodiodi.fi.</Link></p>
        { this.renderErrors() }
        <br />
        <p>Lähettämällä lomakkeen hyväksyt, että annettuja tietoja säilytetään
        projektivuoden loppuun mahdollisesti EU:n ulkopuolella ja käytetään
        sähköpostiviestintään projektivuoden aikana.
        </p>
        <Button type="submit">Lähetä</Button>
      </Form>
    );
  }
}

export default withRouter(RegistrationForm);
