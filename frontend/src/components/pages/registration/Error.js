import React, { Component } from 'react';
import styled from 'styled-components';

const Container = styled.div`
  margin-bottom: 2rem;
`;

const Paragraph = styled.div`
  max-width: 700px;
  flex-grow: 1;

  hr {
    width: 50%;
    text-align: left;
  }
`;

const Title = styled.h3`
  color: ${p => p.theme.color.pink100};
  margin: 0;
  height: 4rem;
`;

class RegistrationError extends Component {
  render() {
    return (
      <Container>
        <Paragraph>
          <Title>Ilmoittautuminen epäonnistui :(</Title>
          <Paragraph>
            Ilmoittautuminen kosahti! Jos lomakkeen uudelleentäyttäminen ei auta, otathan yhteyttä toimitukseen
            osoitteessa <a href="mailto:toimitus@radiodiodi.fi">toimitus@radiodiodi.fi</a>. Pahoittelut häiriöstä!
          </Paragraph>
        </Paragraph>
      </Container>
    );
  }
}

export default RegistrationError;
