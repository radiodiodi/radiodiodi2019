import React, { Fragment, Component } from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

import RegistrationForm from './RegistrationForm';
import Separator from '../../common/Separator';
import { linkToTelegramChat } from '../../../utils';

const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;

  @media screen and (max-width: 700px) {
    flex-direction: column;
  }
`;

const Paragraph = styled.div`
  max-width: 700px;
  flex-grow: 1;

  hr {
    width: 50%;
    text-align: left;
  }
`;

const Title = styled.h3`
  margin: 0;
  height: 4rem;
`;

class Registration extends Component {
  render() {
    return (
      <Fragment>
        <Container>
          <Paragraph>
            <Title>Ilmoittaudu ohjelmantekijäksi</Title>
            Ilmoittaudu tekemään radio-ohjelmaa wappuradio Radiodiodiin! Ohjelmantekeminen ja kaikki siihen liittyvä
            oheistoiminta on opiskelijoille ilmaista. Jos sinulla on kysymyksiä Radiodiodiin tai ohjelman tekemiseen liittyen,
            käy lukemassa Radiodiodin <Link to="/guide/faq">usein kysytyt kysymykset</Link>.
            <br />
            <br />
            Suunnittelethan ohjelmasi huolella! Tehokasta apua suunnitteluun löydät <Link to="/guide/checklist">ohjelmantekijän checklististä</Link>.
            <br />
            <br />
            Muista myös mainostaa omaa ohjelmaasi somessa! Voit käyttää tunnisteita <a>#radiodiodi</a> ja <a>@radiodiodi</a> tai linkata nettisivullemme <a href="https://radiodiodi.fi">https://radiodiodi.fi</a>.
            <br />
            <br />
            Liity myös Radiodiodin
            Telegram-kanaville: <a title="Telegram-tiedotus" onClick={linkToTelegramChat('radiodiodi')}>
              tiedotus</a> ja <a title="Telegram-keskustelu" onClick={linkToTelegramChat('radiodiodi')}>keskustelu</a>.
            <Separator className="single" size="50%" />
            <p>Radiodiodin ohjelma on yhteisön tuottamaa. Kuka vaan voi ilmoittatutua mukaan tekemään ohjelmaa. Kannustamme monipuolisuuteen ja luovaan hulluuteen.</p>
            <p>Ohjelmavuorot ovat yleensä tunnin tai parin mittaisia. Yöaikaan kuullaan myös pidempää ohjelmaa. Ohjelmaan sisältyy tyypillisesti puhetta, musiikkia sekä Radiodiodin tuottamia sponsorien mainoksia, mutta puitteet sallivat myös esimerkiksi livemusiikin esittämisen.</p>
            <p>Ohjelman teknisestä toteutuksesta vastaa toimitus. Studiossa on ohjelman esiintyjien lisäksi aina tuottaja, joka vastaa lähetysteknisistä asioista. Radio kouluttaa myös kiinnostuneita tuottajia ohjelmantekijöistä, joten oman ohjelman tuottaminen itse on mahdollista.</p>
          </Paragraph>
          <RegistrationForm />
        </Container>
      </Fragment>
    );
  }
}

export default Registration;
