import React, { Fragment, Component } from 'react';
import styled from 'styled-components';

import ImageGallery from '../../common/ImageGallery';
import Player from '../../common/Player';
import Instagram from '../../common/Instagram';
import SponsorReel from '../../common/SponsorReel';
import Calendar from '../../calendar/Calendar';
import Shoutbox from '../../common/Shoutbox';
import Separator from '../../common/Separator';
import TextCarousel from '../../common/TextCarousel';
import { linkToTelegramChat } from '../../../utils';
import VideoPlayer from '../../common/VideoPlayer';

const Container = styled.div`
  display: flex;
  justify-content: space-between;

  @media screen and (max-width: 800px) {
    flex-direction: column;
  }
`;

const Paragraph = styled.div`
  margin: 0 0 1rem;

  @media screen and (max-width: 800px) {
    max-width: 100%;
  }

  @media screen and (min-width: 801px) {
    ${p => p.margin && 'margin-right: 2rem'};
  }
`;

const ColumnContainer = styled.div`
  @media screen and (min-width: 801px) {
    margin: 0 ${p => p.margin ? '2rem' : 0} 1rem 0;
    width: ${p => p.size || '50%'};
  }

  @media screen and (max-width: 800px) {
    order: ${p => p.ontop ? '-1' : null};
  }

  display: flex;
  flex-direction: column;
`;

const Title = styled.h1`
  margin: 0 0 1rem;
`;

const ContentRow = styled.div`
  margin: 0;
`;

const ShoutboxTitle = styled.h2`
  margin: 1rem 0 1rem;
  font-size: 1.5rem;
`;

const ContactUsContainer = styled.div`
  line-height: 1.5;
  text-align: left;
`;

class Frontpage extends Component {
  render() {
    return (
      <Fragment>
        <Title>Wapun 2019 lähetys on päättynyt. Kiitos kuulijoille!</Title>
        <Container>
          <ColumnContainer ontop size="50%">
            <Paragraph margin>
              <Title>Mikä on Radiodiodi?</Title>
              <div>
                Radiodiodi on Otaniemestä ponnistava joka vuosi täysin opiskelijavetoisesti toteutettu kaupallinen wappuradio.
                Radio toteutetaan vuonna 2019 jo kahdeksatta kertaa ja lähetys on käynnissä täydet kaksi viikkoa ennen wappuaattoa.
                Projektiin osallistuu joka vuosi lähes sata vapaaehtoisia taustatyön muodossa ja ohjelmaakin tulee vuosittain tekemään
                kahden lähetysviikon aikana yli 500 vapaaehtoista. Radiota kuuntelee lähetyksen aikana koko pääkaupunkiseutu
                ja lasketulla kuuluvuusalueella asuu noin 400 000 ihmistä!
              </div>
              <br />
              <div>
                Lisätietoa Radiodiodista sekä antoisaa keskustelua löytyy
                Telegram-kanaviltamme&nbsp;<span className="span-anchor" title="Telegram-tiedotus" onClick={linkToTelegramChat('radiodiodi')}>
                  tiedotus</span> sekä <span className="span-anchor" title="Telegram-keskustelu" onClick={linkToTelegramChat('radiodiodichat')}>keskustelu</span>.
              </div>
            </Paragraph>
          </ColumnContainer>
          <ColumnContainer size="50%">
            <Paragraph>
              <Title>Tiesitkö?</Title>
              <TextCarousel />
            </Paragraph>
            <Paragraph>
              <Title>Contact us</Title>
              <ContactUsContainer>
                <strong>Toimitus</strong><br />
                <a href="mailto:toimitus@radiodiodi.fi">toimitus@radiodiodi.fi</a><br />
                <strong>Yritysyhteistyö</strong><br />
                <a href="mailto:yrityssuhteet@radiodiodi.fi">yrityssuhteet@radiodiodi.fi</a><br />
                <strong>Mukaan toimintaan</strong><br />
                <a href="mailto:rekry@radiodiodi.fi">rekry@radiodiodi.fi</a>
              </ContactUsContainer>
            </Paragraph>
          </ColumnContainer>
        </Container>
        <VideoPlayer src="https://www.youtube.com/embed/uublsUz-7Bw" />
        {/* <Calendar /> */}
        <ContentRow>
          <Title>Toimitus 2019</Title>
          <ImageGallery />
        </ContentRow>
        <Instagram />
      </Fragment>
    );
  }
}

export default Frontpage;
