import React, { Component, Fragment } from 'react';
import axios from 'axios';
import BanPanel from '../../admin/BanPanel';
import Ban from '../../admin/Ban';
import Cookie from 'universal-cookie';
import AdminNavbar from '../../admin/AdminNavbar';
import { Count, Column, Container, Log } from '../../admin/Styles';
const cookie = new Cookie();

const BACKEND_URL = process.env.REACT_APP_BACKEND_HTTP_URL;


class AdminBansPage extends Component {
  state = {
    bans: [],
    selected: null,
    loading: true,
    logScrolled: false,
  }

  fetchBans = async () => {
    const { history } = this.props;
    const token = cookie.get('jwt');

    try {
      const resp = await axios.get(`${BACKEND_URL}/admin/users/banned`, {
        headers: {
          'Authorization': token,
        },
      });
      const bans = resp.data.bans;
      this.setState({
        bans,
        loading: false,
      });
    } catch (err) {
      console.error(err);
      if (err.response && err.response.status === 401) {
        cookie.remove('jwt');
        history.push('/login?next=/admin/bans');
        return;
      }

      this.setState({
        loading: false,
        error: 'Error, check console',
      });
    }
  }

  componentDidMount() {
    this.refresh();
  }

  componentDidUpdate = () => {
    const { logScrolled } = this.state;
    // Scroll to bottom of chat log
    if (this.log && !logScrolled) {
      this.log.scrollTop = this.log.scrollHeight;
      this.setState({
        logScrolled: true,
      });
    }
  }

  onSelect = (data, type) => {
    console.log(data);
    this.setState({
      selected: data,
    });
  }

  isSelected = (msg, other) => {
    return msg && other && msg._id === other._id;
  }

  refresh = () => {
    this.fetchBans();
  }

  renderContent = () => {
    const { bans, selected, loading } = this.state;
    const { history } = this.props;
    if (loading) {
      return <h2>Loading...</h2>;
    }

    let banRows, count;
    if (bans.length === 0) {
      banRows = <div>No bans.</div>;
    } else {
      banRows = bans.map((m, index) =>
        <Ban selected={this.isSelected(m, selected)} onSelect={this.onSelect} key={index} data={m} />
      );
      count = banRows.length;
    }

    return (
      <Fragment>
        {count && <Count>Total: {count} ban(s)</Count>}
        <Column>
          <Log innerRef={l => { this.log = l }}>
            {banRows}
          </Log>
        </Column>
        <Column hidden={!selected}>
          <BanPanel refresh={this.refresh} history={history} data={selected} />
        </Column>
      </Fragment>
    );
  }

  render() {
    return (
      <Container>
        <AdminNavbar />
        <h2>Bans</h2>
        { this.renderContent() }
      </Container>
    );
  }
}

export default AdminBansPage;
