import React, { Component, Fragment } from 'react';
import styled from 'styled-components';

const Paragraph = styled.p`
  text-align: left;
`;

const List = styled.ul`
`;

const ColumnContainer = styled.div`
  display: flex;
  flex-direction: row;

  @media screen and (max-width: 1000px) {
    flex-direction: column;
  }

  justify-content: space-between;
`;

const Column = styled.span`
  margin-bottom: 1rem;
`;

const Link = styled.a`
`;

const Title = styled.h2`
  text-align: left;
`;

const AudioPlayerContainer = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: space-between;
  margin: 0 -0.5rem;
`;

const AudioPlayer = styled.span`
  display: flex;
  flex-direction: column;
  margin: 0 0.5rem 1rem;

  @supports (-moz-appearance: none) {
    width: 30%;  /* fixes firefox width bug */
  }

  @media screen and (max-width: 700px) {
    width: 100%;
    flex-direction: row;
    justify-content: space-between;
  }

  @media screen and (max-width: 500px) {
    width: 100%;
    flex-direction: column;
    align-items: center;
  }
`;

const AudioPlayerLabel = styled.label`
  margin-bottom: 0.5rem;

  @media screen and (max-width: 700px) {
    margin-bottom: 0;
    align-self: center;
  }

  @media screen and (max-width: 500px) {
    margin-bottom: 0.5rem;
  }
`;

class Sponsors extends Component {
  render() {
    return (
      <Fragment>
        <Paragraph>Ota yhteyttä yrityssuhdevastaavaamme: <Link href="mailto:yrityssuhteet@radiodiodi.fi">yrityssuhteet@radiodiodi.fi</Link></Paragraph>

        <Title>Mitä meillä on tarjota yrityksille?</Title>
        <Paragraph>Jotta Suomen nopein kuulotelevisio voitaisiin aistia, tarvitaan ensin rahaa ja papereiden pyörittelyä.</Paragraph>
        <Paragraph>Radiodiodi tarjoaa yhteistyökumppaneilleen mahdollisuuden tavoittaa koko pääkaupunkiseudun opiskelijat ja vastavalmistuneet radioaalloilla, sekä koko Suomi netin välityksellä. Pop-up-studiomme sijaitsee Otaniemessä, Aalto-yliopiston kymmenien tuhansien opiskelijoiden ja työntekijöiden keskellä. Näymme opiskelijatapahtumissa ympäri vuoden ja tarjoamme pitkäaikaisen yhteistyökumppanuuden. </Paragraph>
        <Paragraph>Yhteistyön laajuudesta riippuen, voimme tarjota mm. seuraavaa:</Paragraph>

        <ColumnContainer>
          <Column>
          <h4>Ainutlaatuisia opiskelijoihin vetoavia wappuradiomainoksia ja yhteisohjelmia</h4>
          <List>
            <li>Tarjoamme n. puolen minuutin pituisia radiomainoksia</li>
            <li>Tuotamme mainosspotit huomioiden sekä teidän toiveenne, että yleisömme odotukset</li>
            <li>Yhteisohjelma on 1 - 2 tunnin täysin vapaamuotoinen ohjelma, jonka tuottamisessa voimme avustaa</li>
          </List>
          </Column>
          <Column>
          <h4>Näkyvyyttä ja tapahtumia</h4>
          <List>
            <li>Logopaikka nettisivuille, bannereihin ja mainoksiin</li>
            <li>Räätälöityjä somepostauksia</li>
            <li>Näkyvyyttä keskellä Otaniemen wappua lähetyskonteillamme</li>
            <li>Myös ohjelmantekijöiden erinäisiin tapahtumiin on mahdollista päästä mukaan </li>
          </List>
          </Column>
        </ColumnContainer>

        <Paragraph>Kiinnostuitko? Ota yhteyttä!</Paragraph>
        <Paragraph>
          <Link href="mailto:yrityssuhteet@radiodiodi.fi">yrityssuhteet@radiodiodi.fi</Link>
        </Paragraph>
        <Title>Aiempien vuosien mainoksia</Title>

        <AudioPlayerContainer>
          <AudioPlayer>
            <AudioPlayerLabel>Vincit &ndash; ATK-ohjelmoitsija</AudioPlayerLabel>
            <audio controls>
              <source src="https://static.radiodiodi.fi/audio/vincit1.wav" type="audio/wav" />
            </audio>
          </AudioPlayer>

          <AudioPlayer>
            <AudioPlayerLabel>SRV &ndash; Diego &amp; Alberto</AudioPlayerLabel>
            <audio controls>
              <source src="https://static.radiodiodi.fi/audio/srv1.wav" type="audio/wav" />
            </audio>
          </AudioPlayer>

          <AudioPlayer>
            <AudioPlayerLabel>Futurice &ndash; Chilicorn.org</AudioPlayerLabel>
            <audio controls>
              <source src="https://static.radiodiodi.fi/audio/futurice-spiceprogram-eng.wav" type="audio/wav" />
            </audio>
          </AudioPlayer>
        </AudioPlayerContainer>
      </Fragment>
    );
  }
}

export default Sponsors;
