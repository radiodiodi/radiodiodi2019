import React, { Component } from 'react';
import styled from 'styled-components';

const Container = styled.div`
`;

const DurationInput = styled.input`
  padding: 0.5rem;
  margin-left: 1rem;
`;

const InputLabel = styled.label`
  display: flex;
  justify-content: flex-start;
  align-items: center;
`;

const Subtitle = styled.h4`
`;

const AmountContainer = styled.div`
  padding: 1rem 2rem;
`;

const Row = styled.div`
  line-height: 1.5rem;
`;

class Programme extends Component {
  state = {
    duration: 2,
  }

  onInputChange = evt => {
    this.setState({
      duration: evt.target.value,
    });
  }

  calculateAmounts = hours => {
    const calc = perc => {
      const num = perc * hours;
      const wholes = Math.floor(num);
      const decimals = num - wholes;
      const mins = Math.floor(decimals * 60);

      const hoursText = wholes === 1 ? 'tunti' : 'tuntia';
      const minsText = mins === 1 ? 'minuutti' : 'minuuttia';

      const hourPart = wholes > 0 ? `${wholes} ${hoursText}` : '';
      const minutePart = mins > 0 ? `${mins} ${minsText}`: '';
      return `${hourPart} ${minutePart}`;
    };

    return {
      ads: calc(0.05),
      speech: calc(0.50),
      music: calc(0.35),
      jingles: calc(0.10),
    }
  }

  render() {
    const { duration } = this.state;
    const amounts = this.calculateAmounts(duration);
    return (
      <Container>
        <h3>Ohjelma</h3>
        <p>
        Tavanomainen radio-ohjelma kestää kaksi tuntia.
        </p>
        <InputLabel>
          Syötä ohjelmasi kesto (tunneissa):
          <DurationInput
            type="number"
            value={duration}
            onChange={this.onInputChange}
            min={0}
          />
        </InputLabel>

        <Subtitle>Ohjelmasi rakenne voisi näyttää esimerkiksi tältä:</Subtitle>
        <AmountContainer>
          <Row>Puhetta: {amounts.speech}</Row>
          <Row>Musiikkia: {amounts.music}</Row>
          <Row>Jinglejä: {amounts.jingles}</Row>
          <Row>Mainoksia: {amounts.ads}</Row>
        </AmountContainer>
      </Container>
    );
  }
}

export default Programme;