import React, { Component } from 'react';
import styled from 'styled-components';
import { Route, Redirect, Switch, Link as ReactLink} from 'react-router-dom';
import FAQ from './FAQ';
import Programme from './Programme';
import Checklist from './Checklist';

const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;

const Link = styled(ReactLink)`
  margin-right: 1rem;
`;

const Rectangle = styled.div`
`;

class Guide extends Component {
  render() {
    const { match } = this.props;
    return (
      <Container>
        <h1>Ohjelmantekijän opas</h1>
        <Rectangle>
          <Link to={`${match.url}/checklist`}>Checklist</Link>
          <Link to={`${match.url}/faq`}>UKK</Link>
          <Link to={`${match.url}/programme`}>Ohjelma</Link>
        </Rectangle>
        <Switch>
          <Route path={`${match.url}/checklist`} component={Checklist} />
          <Route path={`${match.url}/faq`} component={FAQ} />
          <Route path={`${match.url}/programme`} component={Programme} />
          <Redirect to={`${match.url}/faq`} />
        </Switch>
      </Container>
    );
  }
}

export default Guide;