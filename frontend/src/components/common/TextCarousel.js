import Carousel from 'nuka-carousel';
import React from 'react';
import styled from 'styled-components';

/* https://github.com/FormidableLabs/nuka-carousel/issues/457#issuecomment-460695421 */
const CAROUSEL_HEIGHT = '60px';

const CarouselWrapper = styled.div`
  height: ${CAROUSEL_HEIGHT};
  margin-right: 2rem;
  margin-bottom: 0;

  strong {
    color: ${p => p.theme.color.pink100};
  }
`;

export default class extends React.Component {
  render() {
    return (
      <CarouselWrapper>
        <Carousel withoutControls autoplay autoplayInterval={5000} wrapAround pauseOnHover width="100%" heightMode="current">
          <div>Radiodiodi palkittiin Hugo-gaalassa 2018 Aallon&nbsp;
            <a href="https://ayy.fi/blog/2018/12/13/hugo-gaala-palkitsi-aalto-yhteison-pioneereja/"><strong>parhaana yhteisönrakentajana</strong></a>.
          </div>
          <div>Vuonna 2018 tuotettiin <strong>154</strong>&nbsp;radio-ohjelmaa.</div>
          <div>
            Vuonna 2018 Radio lauloi kaksi wappua edeltävää viikkoa, eli yhteensä&nbsp;<strong>342</strong>&nbsp;tuntia,
            <strong>20&nbsp;520</strong>&nbsp;minuuttia tai&nbsp;<strong>1 231 200</strong>&nbsp;sekuntia.
          </div>
        </Carousel>
      </CarouselWrapper>
    );
  }
}