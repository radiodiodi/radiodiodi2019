import React, { Component } from 'react';
import axios from 'axios';
import InstagramGallery from './InstagramGallery';

class Instagram extends Component {
  constructor() {
    super();
    this.state = {
      images: [],
    };

    this.fetchImages = this.fetchImages.bind(this);
    this.fetchImages();
  }

  async fetchImages() {
    const count = process.env.REACT_APP_INSTAGRAM_IMAGE_COUNT;
    const url = `${process.env.REACT_APP_BACKEND_HTTP_URL}/api/instagram_feed?count=${count}`;

    try {
      const resp = await axios.get(url);
      const data = resp.data;

      const imageLinks = data.data;

      this.setState({
        images: imageLinks,
      });
    } catch (err) {
      this.setState({
        error: true,
      });
      console.log(err);
    }
  }

  render() {
    return !this.state.error
      ? <InstagramGallery objs={ this.state.images } />
      : <InstagramGallery error />;
  }
}

export default Instagram;