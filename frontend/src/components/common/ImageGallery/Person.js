import React, { Component } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import FadeImage from '../FadeImage';

const Guy = styled.div`
  width: 100%;
  margin: 0 1% 1rem;

  @media screen and (min-width: 701px) {
    width: calc((100% - 2rem) / 2);
    margin-right: auto;
  }

  @media screen and (min-width: 1001px) {
    width: calc((100% - 3rem) / 3);
  }

  text-align: left-align;

  div {
    margin: 0.5rem 0;
  }
`;

const PersonHeader = styled.div`
  color: ${p => p.theme.color.white100};
  font-size: 1.2rem;
  font-weight: 900;
`;

const PersonRole = styled.div`
  color: ${p => p.theme.color.blue100};
  text-transform: uppercase;
  letter-spacing: 1.5px;
`;

const Img = styled(FadeImage)`
  max-width: 100%;
  ${p => p.css};
`;

const Desc = styled.p`
  text-align: right;
  font-style: italic;
`;

class Person extends Component {
  static propTypes = {
    name: PropTypes.string,
    img: PropTypes.any,
    role: PropTypes.string,
    desc: PropTypes.string,
    mail: PropTypes.string,
  }

  render() {
    const { name, role, img, desc} = this.props;

    return (
    <Guy>
      <PersonHeader>{ name }</PersonHeader>
      <PersonRole>{ role }</PersonRole>
      <Img src={ img } alt="" />
      <Desc>{ desc }</Desc>
    </Guy>
    )
  };
}

export default Person;