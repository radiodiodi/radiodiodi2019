import React, { Component } from 'react';
import styled from 'styled-components';

import Person from './Person';

const Gallery = styled.section`
`;

const GalleryInner = styled.div`
  display: flex;
  flex-flow: row wrap;
  justify-content: space-around;
  margin: 0 -1% 2rem;
`;

const IMG_URL = "https://static.radiodiodi.fi/img/2019/toimitus/thumbnail"

const people = [
  { name: 'Rosa', role: 'Päätoimittaja', desc: 'Kapellimestari aikatauluopuksella ja radioantennilla varustettuna. Toimii myös keppinä.', img: `${IMG_URL}/rosa.jpg`, mail: 'pt@radiodiodi.fi' },
  { name: 'Heidi', role: 'Markkinointivastaava', desc: 'Somejen spämmiminen ja ylipäätään Wappuradiosta mouhoaminen aina, kun tulee tilaisuus.', img: `${IMG_URL}/heidi.jpg`, mail: 'heidi@radiodiodi.fi' },
  { name: 'Saku', role: 'Studiopäällikkö', desc: 'Lähetysäänen vastuuhenkilö. Studiokontin rankaisija. The signal chain calls me daddy.', img: `${IMG_URL}/saku.jpg`, mail: 'saku@radiodiodi.fi' },
  { name: 'Jutta', role: 'Yrityssuhdevastaava', desc: 'Rahahanojen Puuha-Pete. Dolla dolla bill y\'all 💰💰💰', img: `${IMG_URL}/jutta.jpg`, mail: 'jutta@radiodiodi.fi' },
  { name: 'Jan', role: 'Oltermanni', desc: 'Ei noin, vaan näin. Parrakas mouho, jolla on jo muutama radiotuotanto vyön alla.', img: `${IMG_URL}/jan.jpg`, mail: 'jan@radiodiodi.fi' },
  { name: 'Ville', role: 'Graafikko', desc: 'Kuvat ja kulma-aurinko on se mun juttu.', img: `${IMG_URL}/ville.jpg`, mail: 'ville@radiodiodi.fi' },
  { name: 'Elias', role: 'Rakennusmestari', desc: 'Kontit paikalle, sähköt kiinni ja lähetys käyntiin.', img: `${IMG_URL}/elias.jpg`, mail: 'elias@radiodiodi.fi' },
  { name: 'Juhana', role: 'IT-vastaava', desc: 'Paijaan servereitä ja hajoan webstackiin.', img: `${IMG_URL}/keksi.jpg`, mail: 'juhana@radiodiodi.fi' },
  { name: 'Aarni', role: 'Web-vastaava', desc: 'Täyttä pinoamista, Agility level 99.', img: `${IMG_URL}/arska.jpg`, mail: 'aarni@radiodiodi.fi' },
];

class ImageGallery extends Component {
  render() {
    return (
      <Gallery>
        <GalleryInner>
          { people.map((person, index) =>
            <Person key={index} {...person} />) }
        </GalleryInner>
      </Gallery>
    );
  }
}

export default ImageGallery;
