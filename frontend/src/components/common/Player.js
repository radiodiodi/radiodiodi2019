import React, { Component, Fragment } from 'react';
import styled from 'styled-components';
import { fetchNowPlayingProgramme, fetchCurrentSong } from '../../utils';

import playIcon from '../../svg/play.svg';
import pauseIcon from '../../svg/pause.svg';

const Container = styled.div`
  padding: 1rem;
  background-color: ${p => p.theme.color.white10};
  margin-bottom: 1rem;

  @media screen and (max-width: 700px) {
    margin: 1rem 0 1rem;
  }
`;

const PlayButton = styled.img`
  width: 3rem;
  padding: 0.5rem 0.5rem 0 0;
  cursor: pointer;
`;

const Line = styled.div`
  margin: 0;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  margin-top: ${p => p['margin-top'] ? '1rem' : '0'};
`;

const Header = styled.h4`
  font-size: 18px;
  border-bottom: 1px solid ${p => p.theme.color.pink};
  margin: 0 0 0.5rem 0;
`;

const AudioElement = styled.audio`
  display: hidden;
`;

const NowPlayingText = styled.div`
  padding-right: 0.5rem;
  white-space: nowrap;
`;

const NowPlayingValue = styled.i`
  text-align: right;
`;

const OwnPlayer = styled.div`
  margin-top: 1rem;
`;

class Player extends Component {
  constructor() {
    super();
    this.state = {
      playing: false,
    };

    this.fetchNowPlaying = this.fetchNowPlaying.bind(this);
    this.fetchCurrentSong = this.fetchCurrentSong.bind(this);

    window.setInterval(this.fetchNowPlaying, 10000); // 10 seconds interval
    this.fetchNowPlaying();
    window.setInterval(this.fetchCurrentSong, 10000); // 10 seconds interval
    this.fetchCurrentSong();
  }

  playPause = () => {
    const { playing } = this.state;

    if (!playing) {
      this.audio.play();
    } else {
      this.audio.pause();
    }

    this.setState({
      playing: !playing,
    });
  }

  async fetchCurrentSong() {
    let currentSong;
    try {
      currentSong = await fetchCurrentSong();
    } catch (error) {
      console.log(error);
    }

    if (!currentSong || !currentSong.title || !currentSong.artist) {
      this.setState({
        title: null,
        artist: null,
      });
      return;
    }

    this.setState({
      title: currentSong.title,
      artist: currentSong.artist,
    });
  }

  async fetchNowPlaying() {
    let programme;
    try {
      programme = await fetchNowPlayingProgramme();

    } catch (error) {
      console.log(error);
    }

    if (!programme || !programme.title) {
      this.setState({
        programme: null,
      });
      return;
    }

    this.setState({
      programme: programme.title,
    });
  }

  renderCurrentSong = () => {
    const { title, artist } = this.state;

    if (title && artist) {
      return (
        <Line margin-top>
          <NowPlayingText>Now playing:</NowPlayingText>
          <NowPlayingValue>{artist} - {title}</NowPlayingValue>
        </Line>
      );
    } else {
      return null;
    }
  }

  render() {
    const { playing, programme } = this.state;
    const icon = !playing ? playIcon : pauseIcon;
    const currentProgramme = programme || 'Radiodiodi';

    return (
      <Fragment>
        <Container>
          <AudioElement preload="none" innerRef={audio => { this.audio = audio } }>
            <source src="https://virta.radiodiodi.fi/ogg" type="audio/ogg" />
            <source src="https://virta.radiodiodi.fi/mp3" type="audio/mpeg" />
          </AudioElement>
          <Header>Kuuntele radiota!</Header>
          <Line><PlayButton onClick={this.playPause} src={icon} /> <h4>{currentProgramme}</h4></Line>
          { this.renderCurrentSong() }
        </Container>
        <OwnPlayer>Kuuntele omassa soittimessa: <a href="https://static.radiodiodi.fi/radiodiodi.m3u">radiodiodi.m3u</a></OwnPlayer>
      </Fragment>

    );
  }
};

export default Player;
