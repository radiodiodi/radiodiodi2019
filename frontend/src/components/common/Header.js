import React, { Fragment, Component } from 'react';
import styled from 'styled-components';
import { Link as ReactLink } from 'react-router-dom';
import Slogan from './Slogan';
import { linkToTelegramChat } from '../../utils';

const HeaderContainerLarge = styled.header`
  display: none;

  @media screen and (min-width: 1001px) {
    display: flex;
    flex-flow: column nowrap;
    align-items: flex-start;
  }
`;

const HeaderContainerMedium = styled.header`
  display: none;

  @media screen and (min-width: 701px) and (max-width: 1000px) {
    display: flex;
    flex-flow: column nowrap;
    align-items: flex-start;
  }
`;

const HeaderContainerSmall = styled.header`
  display: none;

  @media screen and (max-width: 700px) {
    display: flex;
    flex-flow: column nowrap;
    align-items: flex-start;
  }
`;

const HeaderGraphicsContainer = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;

  @media screen and (max-width: 700px) {
    justify-content: center;
    align-items: center;
    flex-flow: column nowrap;
  }
`;

const HeaderNavContainer = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;
`;

const Logo = styled.img`
  margin: 0.5rem 0;
  max-height: 270px;
  width: 20vw;
  min-width: 175px;
`;

const SocialMediaButton = styled.i`
  font-size: 2rem;
`;

const SocialMediaLink = styled.a`
  color: ${p => p.theme.color.blue100};
  transition: color 0.2s ease;

  &:hover {
    color: ${p => p.theme.color.pink100};
  }

  margin: 0 0.5rem 0;
`;

const SocialMediaContainer = styled.span`
  display: flex;
  flex-flow: row nowrap;
  align-items: center;
`;

const Link = styled(ReactLink)`
  color: ${p => p.color || p.theme.color.blue100};
  margin-left: 0.5rem;
  transition: color 0.3s ease;

  &:hover {
    color: ${p => p.theme.color.pink100};
    text-decoration: none;
  }
`;

const HorizontalMenu = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
`;

const VerticalMenu = styled.div`
  margin-top: 0.5rem;
  display: ${p => p.expanded ? 'flex' : 'none'};
  flex-direction: column;
  align-items: flex-end;
  line-height: 1.8;
`;

const BurgerButton = styled.div`
  display: flex;
  cursor: pointer;
  font-size: 1.8rem;
  padding: 0.5rem;
  margin-right: -0.5rem;
  color: ${p => p.active ? p.theme.color.blue100 : p.theme.color.white100};
`;

const Social = () => (
  <SocialMediaContainer>
    <SocialMediaLink target="_blank" rel="noopener noreferrer" title="Instagram" float href="https://instagram.com/radiodiodi">
      <SocialMediaButton className="fab fa-instagram" />
    </SocialMediaLink>
    <SocialMediaLink target="_blank" rel="noopener noreferrer" title="Facebook" href="https://www.facebook.com/radiodiodi">
      <SocialMediaButton className="fab fa-facebook" />
    </SocialMediaLink>
    <SocialMediaLink title="Telegram-keskustelu" onClick={linkToTelegramChat('radiodiodichat')}>
      <SocialMediaButton className="fab fa-telegram" />
    </SocialMediaLink>
    <SocialMediaLink title="Telegram-tiedotus" onClick={linkToTelegramChat('radiodiodi')}>
      <SocialMediaButton className="fab fa-telegram" />
    </SocialMediaLink>
  </SocialMediaContainer>
);

class Header extends Component {
  constructor() {
    super();
    this.state = {
      mobileMenuExpanded: false,
    };
  }

  toggleMobileMenuExpanded = () => {
    this.setState(prevState => ({
      mobileMenuExpanded: !prevState.mobileMenuExpanded,
    }));
  }

  renderMenuItems = () => {
    return (
      <Fragment>
        <Link to="/">Etusivu</Link>
        {/* <Link to="/ilmo">Ilmoittaudu</Link> */}
        <Link to="/sponsors">Yhteistyökumppaniksi?</Link>
        <Link to="/guide">Ohjelmantekijän opas</Link>
        <Link to="/library">Musiikkikirjasto</Link>
        <Link color="white" to="/english">In English</Link>
      </Fragment>
    )
  }

  render() {
    const { mobileMenuExpanded } = this.state;
    const logoURL = `${process.env.REACT_APP_STATIC_URL}/img/2019/logo.png`;

    return (
      <Fragment>
        { /* Desktop header */ }
        <HeaderContainerLarge>
          <HeaderGraphicsContainer>
            <Link to="/">
              <Logo src={logoURL} />
            </Link>
            <Slogan freq={"90.3 MHz"}></Slogan>
          </HeaderGraphicsContainer>
          <HeaderNavContainer>
            <Social />
            <HorizontalMenu>
              {this.renderMenuItems()}
            </HorizontalMenu>
          </HeaderNavContainer>
        </HeaderContainerLarge>

        { /* Medium (tablet) header */}
        <HeaderContainerMedium>
          <HeaderGraphicsContainer>
            <Link to="/">
              <Logo src={logoURL} />
            </Link>
            <Slogan freq={"90.3 MHz"}></Slogan>
          </HeaderGraphicsContainer>
          <HeaderNavContainer>
            <Social />
            <HorizontalMenu>
              <BurgerButton active={mobileMenuExpanded} onClick={this.toggleMobileMenuExpanded}>
                <i className="fa fa-bars" />
              </BurgerButton>
            </HorizontalMenu>
          </HeaderNavContainer>
        </HeaderContainerMedium>

        { /* Small (phone) header */}
        <HeaderContainerSmall>
          <HeaderGraphicsContainer>
            <Link to="/">
              <Logo src={logoURL} />
            </Link>
            <Slogan freq={"90.3 MHz"}></Slogan>
          </HeaderGraphicsContainer>
          <HeaderNavContainer>
            <Social />
            <HorizontalMenu>
              <BurgerButton active={mobileMenuExpanded} onClick={this.toggleMobileMenuExpanded}>
                <i className="fa fa-bars" />
              </BurgerButton>
            </HorizontalMenu>
          </HeaderNavContainer>
        </HeaderContainerSmall>

        <VerticalMenu expanded={mobileMenuExpanded}>
          { this.renderMenuItems() }
        </VerticalMenu>
      </Fragment>
    );
  }
}

export default Header;