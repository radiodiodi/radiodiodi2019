import React, { Fragment, PureComponent } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import FadeImage from '../FadeImage';
import ImageContainer from './ImageContainer';
import { shortenText } from '../../../utils'

const Card = styled.div`
  align-self: flex-start;
  margin: 0.5rem;
  margin-left: ${p => p.error ? 'auto' : ''};
  margin-right: ${p => p.error ? 'auto' : ''};
  width: calc(100% - 1rem);
  border-top: 4px solid ${p => p.theme.color.blue100};

  @media screen and (min-width: 600px) {
    width: calc((100% - 2rem) / 2);
  }

  @media screen and (min-width: 1000px) {
    width: calc((100% - 3rem) / 3);
  }

  a {
    text-decoration: none;
    color: ${p => p.theme.color.white100};

    &:hover {
      text-decoration: none;
    }
  }
`;

const Image = styled(FadeImage) `
  max-width: 100%;
`;

const Caption = styled.div`
  text-overflow: ellipsis;
  width: 100%;
`;

const Text = styled.span`
`;

const LikeContainer = styled.div`
  margin: 0.5rem 0;

  svg {
    color: ${p => p.theme.color.pink100};
  }
`;

class InstagramGallery extends PureComponent {
  static propTypes = {
    objs: PropTypes.arrayOf(PropTypes.any),
    error: PropTypes.bool,
  }

  renderCard = (obj) => {
    return (
      <Card key={obj.id}>
        <a target="_blank" rel="noopener noreferrer" href={obj.link}>
          <Image alt="Instagram-kuva" src={obj.img} />
          <LikeContainer><i className="fas fa-heart"></i> {obj.likes}</LikeContainer>
        </a>
        <Caption>
          <Text>{shortenText(obj.text)}</Text>
        </Caption>
      </Card>
    );
  }

  renderErrorCard = () => {
    return (
      <Card error>
        <Text>Instagram-kuvien lataus epäonnistui!</Text>
      </Card>
    );
  }

  render() {
    const { objs, error } = this.props;

    const images = !error
      ? objs.map(this.renderCard)
      : this.renderErrorCard();

    return (
      <Fragment>
        <h2>#Instagram-feed</h2>
        <ImageContainer images={images} />
      </Fragment>
    );
  }
}

export default InstagramGallery;