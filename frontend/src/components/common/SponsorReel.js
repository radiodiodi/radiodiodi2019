import React, { Component } from 'react';
import styled from 'styled-components';
// import shuffle from 'shuffle-array';
import FadeImage from './FadeImage';

const futurice = `${process.env.REACT_APP_STATIC_URL}/img/2018/futurice.svg`;
const genelec = `${ process.env.REACT_APP_STATIC_URL}/img/2018/genelec.png`;
const granlund = `${process.env.REACT_APP_STATIC_URL}/img/2019/ilmaveivi.svg`;
const tek = `${process.env.REACT_APP_STATIC_URL}/img/2019/TEK_negative.png`;
const tter = `${process.env.REACT_APP_STATIC_URL}/img/2019/TTER.png`;

const Container = styled.div`
  display: flex;
  flex-direction: column;
`;

const ReelImage = styled(FadeImage)`
  align-self: center;
  max-width: 100%;
  max-height: 100%;
  text-align: justify;
`;

const Title = styled.small`
  text-align: center;
  margin: 0.5rem;
`;

const ImageContainer = styled.div`
  height: 100px;
  width: 300px;
  margin: 0 0 2rem;
  display: flex;
  justify-content: center;
  align-items: center;
  align-self: center;
`;

class SponsorReel extends Component {
  constructor() {
    super();
    this.state = {
      current: 0,
      counter: 0,
      images: [
        futurice,
        genelec,
        granlund,
        tek,
        tter,
      ],
    };

    this.updateCurrent = this.updateCurrent.bind(this);
  }

  componentDidMount() {
    const intervalHandle = window.setInterval(this.updateCurrent, this.props.interval);
    this.setState({
      intervalHandle,
    });
  }

  componentWillUnmount() {
    const { intervalHandle } = this.state;
    window.clearInterval(intervalHandle);
  }

  async updateCurrent() {
    const { counter, images } = this.state;
    this.setState({
      counter: counter + 1,
      current: (counter + 1) % images.length,
    });
  }

  render() {
    const { images, current } = this.state;
    const image = images[current];
    return (
      <Container>
        <Title>Yhteistyössä</Title>
        <ImageContainer>
          <ReelImage
            src={ image }
          />
        </ImageContainer>
      </Container>
    );
  }
}

export default SponsorReel;