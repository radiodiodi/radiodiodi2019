import React, { Component } from 'react';
import styled from 'styled-components';

const BackgroundGraphics = styled.div`
  background-image: url(https://static.radiodiodi.fi/img/2019/double_circle_blue.svg);
  background-size: cover;
  position: fixed;
  z-index: -1;
  width: 3000px;
  height: 3000px;
  top: -1500px;
  right: -2100px;
  opacity: 0.2;
`;

class Background extends Component {
  render() {
    return (
      <BackgroundGraphics />
    );
  }
}

export default Background;
