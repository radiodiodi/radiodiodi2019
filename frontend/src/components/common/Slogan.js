import React, {Component} from 'react';
import styled from 'styled-components';


const phrases = [
  "Korkea taajuus, matala laatu",
  "Matala taajuus, korkea laatu",
  "Etelän halvinta puheaikaa",
  "Suomen nopein kuulotelevisio",
];

const getRandomPhrase = () => {
  return phrases[~~(Math.random() * phrases.length)];
}

const Container = styled.div`
  display: flex;
  flex-flow: column nowrap;
  justify-content: space-around;
  align-items: flex-end;
  margin: 5vh 0;

  @media screen and (max-width: 700px) {
    margin: 3vh 0;
    align-items: center;
  }
`;

const Phrase = styled.div`
  display: flex;
  font-size: 1.5em;
  color: ${p => p.theme.color.blue100};
  margin: 5px 0;

  @media screen and (max-width: 1000px) {
    font-size: 16px;
  }
`;

const Frequency = styled.div`
  display: flex;
  font-size: 2.5em;
  color: ${p => p.theme.color.pink100};
  margin: 5px 0;

  @media screen and (max-width: 1000px) {
    font-size: 24px;
  }
`;

class Slogan extends Component {
  shouldComponentUpdate() {
    return false;   // Frontpage has some component that triggers re-render. Bubble gum fix to re-run of getRandomPhrase()
  }

  render() {
    const phrase = getRandomPhrase();
    const { freq } = this.props;
    return(
    <Container>
      <Phrase>{phrase}</Phrase>
      <Frequency >{freq}</Frequency>
    </Container>
    )
  }
}

export default Slogan;