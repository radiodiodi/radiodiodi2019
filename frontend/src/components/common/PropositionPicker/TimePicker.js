import React, { Component, Fragment } from 'react';
import styled from 'styled-components';

const Widget = styled.select`
  padding: 0.5rem 1rem;
  margin-right: 0.5rem;
  user-select: none;
  font-variant-numeric: tabular-nums;
  font-size: 1rem;
  line-height: 1.3rem;
  border-radius: 0;
  appearance: none;
`;

const leftPad = n => n >= 10 ? `${n}` : `0${n}`;

const generateOptions = () => {
  const times = [...Array(24).keys()].map((n, i) => {
    const time = `${leftPad(n)}:00`;
    return <option key={i} value={time}>{time}</option>;
  });
  return [
    <option key="--:--" value={null}>--:--</option>,
    ...times,
  ];
}

class TimePicker extends Component {
  render() {
    const { time, onChange } = this.props;
    const options = generateOptions();
    return (
      <Fragment>
        <Widget onChange={onChange} value={ time }>
          {options }
        </Widget>
      </Fragment>
    );
  }
}

export default TimePicker;