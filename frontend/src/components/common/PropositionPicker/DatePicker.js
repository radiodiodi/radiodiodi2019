import React, { Component, Fragment } from 'react';
import styled from 'styled-components';
import { CalendarIcon } from '../Icons';

const Widget = styled.div`
  display: flex;
  align-items: center;
  padding: 0.5rem 0;
  user-select: none;
  font-variant-numeric: tabular-nums;
  cursor: pointer;

  &:hover {
    font-weight: bold;
  }
`;

const IconContainer = styled.span`
  display: flex;
  margin-right: 10px;

  svg {
    color: ${p => p.theme.color.blue100};
    width: 16px;
    height: 16px;
  }
`;

class DatePicker extends Component {
  render() {
    const { date, onClick } = this.props;
    const component = (
      <Fragment>
        <IconContainer><CalendarIcon /></IconContainer>
        {date || 'Valitse päivämäärä' }
      </Fragment>
    )
    return (
      <Fragment>
        <Widget onClick={onClick}>
          { component }
        </Widget>
      </Fragment>
    );
  }
}

export default DatePicker;