import React, { Component, Fragment } from 'react';
import styled from 'styled-components';
import CalendarWidget from './CalendarWidget';
import DatePicker from './DatePicker';
import TimePicker from './TimePicker';
import { CrossIcon } from '../Icons';

const PropositionRow = styled.span`
  padding: 0.5rem 0 0.5rem 1rem;
  margin-bottom: 1rem;
  border-left: 4px solid ${p => p.theme.color.blue100};
  display: flex;
  justify-content: space-between;

  @media screen and (max-width: 600px) {
    flex-direction: column;
  }
`;

const Column = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  flex: ${p => p.flex ? '1' : '0.5'};

  @media screen and (max-width: 600px) {
    margin-top: 0.5rem;
  }
`;

const Label = styled.label`
  font-size: 0.9rem;
`;

const RemoveButton = styled.div`
  display: flex;
  max-width: 30px;
  margin: 0.5rem;
  align-self: flex-end;
  cursor: pointer;

  svg {
    width: 20px;
    height: 20px;
    color: ${p => p.theme.color.pink100};
  }
`;

class Proposition extends Component {
  constructor() {
    super();
    this.state = {
      showCalendar: true,
    };
  }

  onDatePickerClick = () => {
    this.setState({
      showCalendar: !this.state.showCalendar,
    });
  }

  onDateSelect = (dateDay) => {
    const date = `${dateDay}.04.2019`;
    const { onUpdateValue, startTime, endTime } = this.props;
    this.setState({
      showCalendar: false,
    });

    onUpdateValue({ date, startTime, endTime });
  }

  onStartTimeChange = (event) => {
    const startTime = event.target.value;
    const { onUpdateValue, date, endTime } = this.props;
    onUpdateValue({ date, startTime, endTime });
  }

  onEndTimeChange = (event) => {
    const endTime = event.target.value;
    const { onUpdateValue, date, startTime } = this.props;
    onUpdateValue({ date, startTime, endTime });
  }

  remove = () => {
    const { onDelete } = this.props;
    onDelete();
  }

  render() {
    const { showCalendar } = this.state;
    const { date, startTime, endTime } = this.props;
    return (
      <Fragment>
        <PropositionRow>
          <Column flex>
            <Label>Päivämäärä</Label>
            <DatePicker onClick={this.onDatePickerClick} date={ date } />
            <CalendarWidget showOnMobile onDateSelect={this.onDateSelect} show={showCalendar} />
          </Column>
          <Column>
            <Label>Ensimmäinen sopiva aloituskellonaika</Label>
            <TimePicker onChange={this.onStartTimeChange} time={ startTime } />
          </Column>
          <Column>
            <Label>Viimeinen sopiva lopetuskellonaika</Label>
            <TimePicker onChange={this.onEndTimeChange} time={ endTime } />
          </Column>
          <RemoveButton onClick={this.remove} >
            <CrossIcon />
          </RemoveButton>
        </PropositionRow>
        <CalendarWidget showOnDesktop onDateSelect={this.onDateSelect} show={showCalendar} />
      </Fragment>
    );
  }
}

export default Proposition;
