import React, { Component } from 'react';
import styled from 'styled-components';
import { asField } from 'informed';
import Proposition from './Proposition';
import { PlusIcon } from '../Icons';

const Container = styled.div`
  margin: 10px 0;
`;

const AddButton = styled.div`
  display: flex;
  max-width: 1rem;
  margin-right: 0.8rem;

  svg {
    height: 20px;
    width: 20px;
    color: ${p => p.theme.color.blue100};
  }
`;

const AddButtonRow = styled.span`
  display: flex;
  cursor: pointer;
  align-items: center;
`;

class PropositionPicker extends Component {
  setPropositionValue = index => (newValue) => {
    const { fieldState, fieldApi } = this.props;
    const { setValue } = fieldApi;
    const prev = [...fieldState.value];
    prev[index] = newValue;
    setValue(prev);
  }

  deleteProposition = index => (newValue) => {
    const { fieldState, fieldApi } = this.props;
    const { setValue } = fieldApi;
    const prev = [...fieldState.value];
    prev.splice(index, 1);
    setValue(prev);
  }

  renderPropositions = () => {
    const { fieldState } = this.props;
    const value = fieldState.value || [];

    const rows = value.map((p, i) => (
      <Proposition
        onUpdateValue={this.setPropositionValue(i)}
        onDelete={this.deleteProposition(i)}
        key={i}
        {...p}
      />
    ));

    return (
      <div>
        { rows }
      </div>
    );
  }

  onAddProposition = () => {
    const { fieldState, fieldApi } = this.props;
    const { setValue } = fieldApi;
    const oldValue = fieldState.value || [];

    // remove nulls
    const value = oldValue.filter(p => !!p.date);

    setValue([
      ...value,
      { date: null, startTime: null, endTime: null},
    ]);
  }

  renderAddButton() {
    return (
      <AddButtonRow onClick={ this.onAddProposition }>
        <AddButton>
          <PlusIcon />
        </AddButton>
        <div>Lisää ehdotus</div>
      </AddButtonRow>
    );
  }

  render() {
    return (
      <Container>
        { this.renderPropositions() }
        { this.renderAddButton() }
      </Container>
    )
  }
}

export default asField(PropositionPicker);
