import React, { Component } from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
  @media screen and (max-width: 600px) {
    display: ${p => p.showOnMobile ? 'inherit' : 'none'};
  }

  @media screen and (min-width: 601px) {
    border-left: 4px solid ${p => p.theme.color.blue100};
    margin: -1rem 0 0.5rem;
    display: ${p => p.showOnDesktop ? 'inherit' : 'none'};
  }
`;

const Container = styled.div`
  padding: 0.5rem 1rem;
  background-color: ${p => p.theme.color.white10};

  @media screen and (max-width: 600px) {
    width: 100%;
    margin-right: 0.5rem;
  }

  @media screen and (min-width: 601px) {
    margin-left: 1rem;
    max-width: 300px;
  }
`;

const TitleContainer = styled.div`
  padding: 0 1rem;
  display: flex;
  justify-content: center;
`;

const Title = styled.h4`
  margin: 0;
`;

const DateContainer = styled.div``;

const Date = styled.div`
  cursor: pointer;

  &:hover {
    color: ${p => p.theme.color.blue100};
  }
`;

const DateHeader = styled.div`
  user-select: none;
`;

const FillerDate = styled.div`
  min-width: 18px;
`;

const DateRow = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 0.5rem 0;
`;

const WeekdayRow = styled(DateRow)`
  padding: 0.5rem 0 0;
`;

const Line = styled.hr`
  border: 1px solid;
`;

class CalendarWidget extends Component {
  renderDate = (date, index) => {
    const { onDateSelect } = this.props;
    if (!date) return <FillerDate />;
    return <Date key={index} onClick={() => onDateSelect(date)}>{date}</Date>
  }

  renderDateHeader = (date, index) => {
    return <DateHeader key={index}>{date}</DateHeader>
  }

  renderCalendar = () => {
    const { showOnDesktop, showOnMobile } = this.props;
    const weekdays = ['ma', 'ti', 'ke', 'to', 'pe', 'la', 'su'].map(this.renderDateHeader);
    const firstRow = [15, 16, 17, 18, 19, 20, 21].map(this.renderDate);
    const secondRow = [22, 23, 24, 25, 26, 27, 28].map(this.renderDate);
    const thirdRow = [29, 30, '', '', '', '', ''].map(this.renderDate);
    return (
      <Wrapper showOnDesktop={showOnDesktop} showOnMobile={showOnMobile}>
        <Container>
          <TitleContainer>
            <Title>Huhtikuu</Title>
          </TitleContainer>
          <DateContainer>
            <WeekdayRow>{ weekdays }</WeekdayRow>
            <Line />
            <DateRow>{ firstRow }</DateRow>
            <DateRow>{ secondRow }</DateRow>
            <DateRow>{ thirdRow }</DateRow>
          </DateContainer>
        </Container>
      </Wrapper>
    );
  }

  render() {
    const { show } = this.props;
    return show ? this.renderCalendar() : null;
  }
}

export default CalendarWidget;